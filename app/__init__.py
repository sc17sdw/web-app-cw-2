from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_uploads import UploadSet, IMAGES, configure_uploads
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)

migrate = Migrate(app, db)
#handles logging

# handles log in
login = LoginManager(app)


file_handler = RotatingFileHandler('logs/app.log', maxBytes=10240,
                                       backupCount=10)
file_handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
file_handler.setLevel(logging.INFO)
app.logger.addHandler(file_handler)

app.logger.setLevel(logging.INFO)
app.logger.info('app startup')
#Using Flask-upload to handle photos uploaded by user
images = UploadSet('images', IMAGES)
configure_uploads(app, images)

#for password encryption
bcrypt = Bcrypt(app)
from app import views, models
