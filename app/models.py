from app import db, login
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from flask_login import UserMixin

#many to many relationship relating events to user if the user has a ticket for them
Tickets = db.Table('Tickets',
    db.Column('event',db.String, db.ForeignKey('events.id')),
    db.Column('username',db.String, db.ForeignKey('user.username'))
)

class User(UserMixin, db.Model):
    username = db.Column(db.String(25), primary_key=True)
    email = db.Column(db.String(320), unique=True)
    password = db.Column(db.String(128), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    tickets = db.relationship('Events', secondary=Tickets, backref = db.backref('Attendees'))
    @login.user_loader
    def load_user(id):
        return User.query.get(id)
    def set_password(self, password):
        """Create hashed password."""
        self._password = bcrypt.generate_password_hash(plaintext_password)
    def check_password(self, password):
        """Check hashed password."""
        return bcrypt.check_password_hash(self.password, plaintext_password)
    def get_id(self):
        return (self.username)
    def __repr__(self):
        return '<User {}>'.format(self.username)


class Events(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    artist = db.Column(db.String(100))
    description = db.Column(db.String(1500))
    location = db.Column(db.String(50))
    venue = db.Column(db.String(50))
    poster = db.Column(db.String(1000))
    capacity = db.Column(db.Integer())
    sold = db.Column(db.Integer())
    price = db.Column(db.Float(8))
    image_filename = db.Column(db.String, default=None, nullable=True)
    image_url = db.Column(db.String, default=None, nullable=True)
