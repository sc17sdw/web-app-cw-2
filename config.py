WTF_CSRF_ENABLED = True
SECRET_KEY = 'a-very-secret-secret'

import os


basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
#SESSION_COOKIE_SECURE = True
#REMEMBER_COOKIE_SECURE = True
#SESSION_COOKIE_HTTPONLY = True
#REMEMBER_COOKIE_HTTPONLY = True
SQLALCHEMY_TRACK_MODIFICATIONS = True
UPLOADS_DEFAULT_DEST = 'app/static/'
UPLOADS_DEFAULT_URL = 'http://localhost:5000/static/'
