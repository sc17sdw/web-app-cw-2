from flask import render_template, flash, request, redirect, url_for
from app import app, db
from models import User, Events, Tickets
from .forms import signInForm, registerForm, eventForm, buyTicketForm
from flask_login import login_user , logout_user , current_user , login_required
from app import images
from werkzeug.utils import secure_filename

#takes data entry from user and adds it to the database. Checking is done in the
# form.

@app.route('/')
@app.route('/events', methods=['GET', 'POST'])
def events():
    events = Events.query.all()
    for event in events:
        print event.image_url
        print event.image_filename
    return render_template("events.html",events=events)

#dynamically create event page based on event id
@app.route('/events/<id>', methods=['GET', 'POST'])
def showEvent(id):
    event = Events.query.filter_by(id=id).first()
    return render_template("showEvents.html",event=event)


@app.route('/profile',methods=['GET', 'POST'])
@login_required
def profile():
    if current_user.is_anonymous:
        flash('You must be signed in to view your profile')
        return redirect(url_for('signIn'))
    user = current_user
    return render_template("profile.html",user=user)

@app.route('/addEvent', methods=['GET', 'POST'])
def addEvent():
    form = eventForm()
    if current_user.is_anonymous:
        flash('You must be logged in to add an event')
        return redirect(url_for('signIn'))
    if form.validate_on_submit():
        filename = images.save(request.files['poster'])
        url = images.url(filename)
        event = Events(title=form.title.data, artist=form.artist.data,
                            location=form.location.data, venue=form.venue.data,
                            description=form.description.data,
                            capacity=form.capacity.data, sold="0",
                            price=form.price.data, image_filename="/static/images/"+filename,
                            image_url="/" +form.title.data)
        db.session.add(event)
        db.session.commit()
        flash("You have added a new event")
        app.logger.info('New Event' + event.title + 'Added')
        return redirect(url_for('events'))
    return render_template("addEvent.html",
                            form = form)

@app.route('/tickets', methods=['GET', 'POST'])
def tickets():
    if current_user.is_anonymous:
        flash('You must be signed in to buy a ticket')
        return redirect(url_for('signIn'))
    #creates iterable object from by querying all events that the current user
    #has tickets for
    events = current_user.tickets
    return render_template("tickets.html",events=events)
@app.route('/ticket/<id>', methods=['GET', 'POST'])
def showTicket(id):
    event = Events.query.filter_by(id=id).first()
    return render_template("ticket.html",event=event,user=current_user)

@app.route('/buyTicket/<id>', methods=['GET', 'POST'])
def buyTicket(id):
    if current_user.is_anonymous:
        flash('You must be signed in to buy a ticket')
        return redirect(url_for('signIn'))
    form = buyTicketForm()
    event = Events.query.filter_by(id=id).first()
#    user = User.query.filter_by(username=current_user.username)
    if form.validate_on_submit():
        event.Attendees.append(current_user)
        db.session.commit()
        flash('Ticket Bought')
        return redirect(url_for('events'))
    app.logger.info('Ticket for ' + event.title + ' purchased by ' + current_user.username)
    return render_template("buyTicket.html",
                            form = form,
                            event=event)

@app.route('/signIn', methods=['GET', 'POST'])
def signIn():
    #if user is already logged in then send to home
    if current_user.is_authenticated:
        flash('You are already signed in')
        return redirect(url_for('events'))
    form = signInForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data, password=form.password.data).first()
        if user is None:    #no match in database
            flash('Incorrect username or password')
            return redirect(url_for('signIn'))
        login_user(user,remember=True)
        flash('Successfully Logged in')
        app.logger.info(user.username + ' signed in')
    return render_template("signIn.html",
                           title="Sign In",
                           form = form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    #if user is already logged in then send to home
    if current_user.is_authenticated:
        flash('You must log out before registering for a new account')
        return redirect(url_for('events'))
    form = registerForm()
    if form.validate_on_submit():
        #if user clicks remember me then details will be stored in cookies
        remember_me = False
        if 'remember_me' in request.form:
            remember_me = True
        #checks if email or password are already in use
        emailCheck = User.query.filter_by(email=form.email.data).first()
        if emailCheck is not None:
            flash("Email already in use")
            return redirect(url_for('register'))
        usernameCheck = User.query.filter_by(username=form.username.data).first()
        if usernameCheck is not None:
            flash("Username already in use")
            return redirect(url_for('register'))
        #adds user to database
        user = User(name=form.name.data, username=form.username.data, email=form.email.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("You have registered")
        app.logger.info('New user ' + user.username + ' registered')
        return redirect(url_for('signIn'))
    app.logger.info(form.errors)
    return render_template("register.html",
                           title="Register",
                           form = form)
#called by the 'x' on completedTasks this deletes the entry from the database
@app.route('/changePassword', methods=['GET', 'POST'])
def changePassword():
    form = registerForm()
    print form.errors
    if form.validate_on_submit():
        flash('Boom')
        user =  current_user
        print user
        user.password = form.password.data
        db.session.add(user)
        db.session.commit()
    app.logger.info('password change from')
    return render_template("changePassword.html",
                           form = form)

@app.route('/logout')
def logOut():
    logout_user()
    return redirect(url_for('signIn'))
