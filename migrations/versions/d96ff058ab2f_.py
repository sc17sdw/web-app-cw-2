"""empty message

Revision ID: d96ff058ab2f
Revises: 
Create Date: 2019-12-17 23:32:40.804504

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd96ff058ab2f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('events',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=500), nullable=True),
    sa.Column('artist', sa.String(length=500), nullable=True),
    sa.Column('description', sa.String(length=2000), nullable=True),
    sa.Column('location', sa.String(length=500), nullable=True),
    sa.Column('venue', sa.String(length=500), nullable=True),
    sa.Column('poster', sa.String(length=1000), nullable=True),
    sa.Column('capacity', sa.String(length=500), nullable=True),
    sa.Column('sold', sa.String(length=500), nullable=True),
    sa.Column('price', sa.Float(precision=10), nullable=True),
    sa.Column('image_filename', sa.String(), nullable=True),
    sa.Column('image_url', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user',
    sa.Column('username', sa.String(length=25), nullable=False),
    sa.Column('email', sa.String(length=320), nullable=True),
    sa.Column('password', sa.String(length=128), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=False),
    sa.PrimaryKeyConstraint('username'),
    sa.UniqueConstraint('email')
    )
    op.create_table('Tickets',
    sa.Column('event', sa.String(), nullable=True),
    sa.Column('username', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['event'], ['events.id'], ),
    sa.ForeignKeyConstraint(['username'], ['user.username'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('Tickets')
    op.drop_table('user')
    op.drop_table('events')
    # ### end Alembic commands ###
