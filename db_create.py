
from config import SQLALCHEMY_DATABASE_URI
from app import db
from models import User, Events
import os.path

#create database
db.create_all()

#initialise database with data
user1 = User(username = 'test', email = 'test@gmail.com', password = 'tester', name = 'test man' )
user2 = User(username = 'bobross', email = 'bobross@gmail.com', password = 'clouds', name = 'bob ross' )
user3 = User(username = 'turing', email = 'turing@gmail.com', password = 'enigma', name = 'Alan Turing' )
db.session.add(user1)
db.session.add(user2)
db.session.add(user3)

db.session.commit
