from flask_wtf import Form
from wtforms import validators, IntegerField, StringField, SubmitField, TextAreaField, DecimalField
from wtforms.validators import DataRequired, Regexp, ValidationError, Email, Length, EqualTo, NumberRange
from wtforms.widgets import TextArea, PasswordInput
from flask_wtf.file import FileField, FileAllowed, FileRequired
from app import images

class signInForm(Form):
    username = StringField('Enter Username',validators=[DataRequired(), Length(max=25)], widget=TextArea())
    password = StringField('Enter Password',validators=[DataRequired()], widget=PasswordInput(hide_value=False))
    submit = SubmitField('Submit')

class registerForm(Form):
    name = StringField('Enter Full Name', validators=[DataRequired(), Length(max=25)], widget=TextArea())
    email = StringField('Enter Email', validators=[DataRequired(), Email()], widget=TextArea())
    username = StringField('Enter Username',validators=[DataRequired(), Length(max=25)], widget=TextArea())
    password = StringField('Enter Password', validators=[DataRequired(), EqualTo('repeatPassword', message='Passwords must match'), Length(max=50)], widget=PasswordInput(hide_value=False))
    repeatPassword = StringField('Repeat Password',validators=[DataRequired()], widget=PasswordInput(hide_value=False))
    submit = SubmitField('Submit')

class eventForm(Form):
    title = StringField('Enter Title',validators=[DataRequired(), Length(max=100)], widget=TextArea())
    artist = StringField('Enter Artist(s)', validators=[DataRequired(), Length(max=100)],widget=TextArea())
    description =  StringField('Enter Description (2000 Characters)', validators=[DataRequired(), Length(max=1500)],widget=TextArea())
    location = StringField('Enter Locations',validators=[DataRequired(), Length(max=50)], widget=TextArea())
    venue = StringField('Enter Venue',validators=[DataRequired(), Length(max=50)], widget=TextArea())
    capacity = IntegerField('Enter capacity',validators=[DataRequired()], widget=TextArea())
    price = DecimalField('Enter Price',validators=[DataRequired()],widget=TextArea())
    poster = FileField('Upload Artwork (2:1 aspect ratio recommended)',validators=[FileRequired(), FileAllowed(images, 'Images only!')])

class buyTicketForm(Form):
    name = StringField('Enter Full Name', validators=[DataRequired()], widget=TextArea())
    cardNo = IntegerField('Enter Card Number', validators=[DataRequired(),
                                                    NumberRange(min=1000000000000000,
                                                    max=9999999999999999,
                                                    message="Must be a 16 digit number")],  widget=TextArea())
    cvc = IntegerField('Enter Security Code', validators=[DataRequired(),
                                                    NumberRange(min=100,
                                                    max=999,
                                                    message="Must be a 3 digit number")],
                                                    widget=TextArea())
