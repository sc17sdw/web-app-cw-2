import os
import unittest

from config import basedir
from app import app, db
from app.models import User, Events
from sqlalchemy.exc import IntegrityError
class pageTests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
            os.path.join(basedir,'test.db')
        self.app = app.test_client()
        db.create_all()
        self.assertEquals(app.debug, False)
    def tearDown(self):
        db.session.remove()
        db.drop_all()
    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertIn(b'Tickets', response.data)

    # Tests whether the register page loads and checks that the form has been
    # loaded correctly
    def test_register_page(self):
        response = self.app.get('/register')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Enter Email',response.data)
        self.assertIn(b'Enter Full Name',response.data)
        self.assertIn(b'Repeat Password',response.data)

    #tests that the register form submits
    def test_register_user(self):
        response = self.app.post(
            '/register',
            data=dict(name='test man', username='tester', email='testdata@gmail.com', password='tester', repeatPassword='tester'),
            follow_redirects=True)
        url = response.data
        self.assertIn(b'register', response.data)

class dbTests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
        os.path.join('test.db')
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        self.assertEquals(app.debug, False)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
    #adds user to database then checks to see whether they have been added
    def test_add_user(self):
        u = User(name='test man', username='tester', email='testdata@gmail.com', password='tester')
        db.session.add(u)
        db.session.commit()
        user = User.query.filter_by(username='tester').first()
        assert user.username == 'tester'

    #adds two users with the same username
    @unittest.expectedFailure
    def test_duplicate_user(self):
        u = User(name='test man', username='tester', email='testdata@gmail.com', password='tester')
        db.session.add(u)
        db.session.commit()
        u2 = User(name='test man2', username='tester', email='testdata2@gmail.com', password='tester2')
        db.session.add(u2)
        db.session.commit()
        db.session.rollback()

    def test_hashed_password(self):
        u = User(name='test man', username='tester', email='testdata@gmail.com', password='tester')
        db.session.add(u)
        db.session.commit()
        user = User.query.filter_by(username='tester').first()
        assert u.password == user.password

    def test_change_password(self):
        u = User(name='test man', username='tester', email='testdata@gmail.com', password='tester')
        db.session.add(u)
        db.session.commit()
        u.password = 'testy'
        db.session.add(u)
        db.session.commit()
        user = User.query.filter_by(username='tester').first()
        assert user.password == 'testy'

        #mimics sign in check
    def test_find_user(self):
        u = User(name='test man', username='tester', email='testdata@gmail.com', password='tester')
        db.session.add(u)
        db.session.commit()
        findUser = User.query.filter_by(username="test man", password="tester")
        assert findUser != None

    @unittest.expectedFailure
    def test_username_long(self):
        u = User(name='test man', username='12345678910111213141516171819202122232425', email='testdata@gmail.com', password='tester')
        db.session.add(u)
        db.session.commit()

    def test_add_event(self):
        event = Events(title="test", artist="test",
                            location="test", venue="test",
                            description="test",
                            capacity="test", sold="0",
                            price=3, image_filename="/static/images/"+"test",
                            image_url="/" +"test")
        db.session.add(event)
        db.session.commit()
        event1 = Events.query.filter_by(title='test').first()
        assert event.title == 'test'

    @unittest.expectedFailure
    def test_duplicate_event(self):
        event = Events(id=1,title="test", artist="test",
                            location="test", venue="test",
                            description="test",
                            capacity="test", sold="0",
                            price=3, image_filename="/static/images/"+"test",
                            image_url="/" +"test")
        db.session.add(event)
        db.session.commit()
        event2 = Events(id=1, title="test", artist="test",
                            location="test", venue="test",
                            description="test",
                            capacity="test", sold="0",
                            price=3, image_filename="/static/images/"+"test",
                            image_url="/" +"test")
        db.session.add(event2)
        db.session.commit()

        #tests many to many relationship when buying ticket
    def test_buy_ticket(self):
        #add event
        event = Events(id=10,title="test", artist="test",
                            location="test", venue="test",
                            description="test",
                            capacity="test", sold="0",
                            price=3, image_filename="/static/images/"+"test",
                            image_url="/" +"test")
        db.session.add(event)
        db.session.commit()
        #add user
        u = User(name='test man', username='tester', email='testdata@gmail.com', password='tester')
        db.session.add(u)
        db.session.commit()
        #add user as an attendee to events
        event.Attendees.append(u)
        db.session.commit()
        ticket = u.tickets

        assert ticket == [Events.query.filter_by(id=10).first()]
if __name__ == '__main__':
    unittest.main()
